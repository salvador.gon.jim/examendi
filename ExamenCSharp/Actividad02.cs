﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Actividad02
    {
        private Automovil automovil;
        public Actividad02() {
           automovil = new Automovil("Seat", "Panda", 2008);
        }
        public void ejecutar()
        {
            Console.WriteLine();
            Console.WriteLine("Ejecutando actividad 2.");
            automovil.informacionVehiculo();
            Console.WriteLine("Fin actividad 2.");
            Console.WriteLine();
        }
    }
}
