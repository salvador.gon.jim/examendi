﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Automovil : Vehiculo
    {
        private int anio;
        public Automovil(string marca, string modelo, int anio) : base (marca, modelo)
        {
            this.marca = marca;
            this.modelo = modelo;   
            this.anio = anio;
        }
        public override void informacionVehiculo()
        {
            Console.WriteLine(this.marca + " " + this.modelo + " " + this.anio);
        }
    }
}
