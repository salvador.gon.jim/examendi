﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Actividad04
    {
        private string cadena;
        public Actividad04() {
            cadena = "Mas frio que en el cumpleños de pingu";
        }

        public void ejecutar()
        {
            Console.WriteLine();
            Console.WriteLine("Ejecutando actividad 4.");
            cadenaInversa();
            Console.WriteLine();
            Console.WriteLine("Fin actividad 4.");

        }

        public void cadenaInversa()
        {
            for (int i = 1; i < cadena.Length+1; i++)
            {
                Console.Write(cadena.ElementAt(cadena.Length - i));
            }
        }
    }
}
