﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Actividad01
    {
        private int[] numeros;
        private int tamanioArray;
        public Actividad01() {
            tamanioArray = 5;
            numeros = new int[tamanioArray];
            numeros[0] = 1;
            numeros[1] = 5;
            numeros[2] = 9;
            numeros[3] = 12;
            numeros[4] = 4;
        }

        public void ejecutar()
        {
            Console.WriteLine();
            Console.WriteLine("Ejecutando actividad 1.");
            sumar();
            Console.WriteLine("Fin actividad 1.");
            Console.WriteLine();
        }
        private void sumar()
        {
            int suma = 0;
            for (int i = 0; i < numeros.Length; i++)
            {
                suma += numeros[i];
            }
            Console.WriteLine("La suma de todos los numeros es: " + suma);
        }
    }
}
