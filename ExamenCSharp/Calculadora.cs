﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Calculadora
    {
        private void sumar(int numeroPrimero, int numeroSegundo)
        {
            try
            {
                int resultadoSuma = numeroPrimero + numeroSegundo;
                Console.WriteLine("El resultado de la suma es: " + resultadoSuma);
            } catch(Exception e) {
                Console.WriteLine("Error al sumar");
            }
        }
        private void restar(int numeroPrimero, int numeroSegundo)
        {
            try
            {
                int resultadoResta = numeroPrimero - numeroSegundo;
                Console.WriteLine("El resultado de la resta es: " + resultadoResta);
            } catch (Exception e)
            {
                Console.WriteLine("Error al restar");
            }
        }
        private void multiplicar(int numeroPrimero, int numeroSegundo)
        {
            try
            {
                int resultadoMultiplicacion = numeroPrimero * numeroSegundo;
                Console.WriteLine("El resultado de la multiplicacion es: " + resultadoMultiplicacion);
            } catch (Exception e){
                Console.WriteLine("Error al multiplicar.");
            }
        }
        private void dividir(int numeroPrimero, int numeroSegundo)
        {
            try
            {
                int resultadoDivision = numeroPrimero / numeroSegundo;
                Console.WriteLine("El resultado de la division es: " + resultadoDivision);
            } catch (Exception e)
            {
                Console.WriteLine("Error al dividir.");
            }
        }
        public void realizarPruebas(int numeroA, int numeroB, int numeroCero)
        {
            sumar(numeroA, numeroB);
            restar(numeroA, numeroB);
            multiplicar(numeroA, numeroB);
            dividir(numeroA, numeroB);
            Console.WriteLine("Dividiendo numeroA / numeroCero");
            dividir(numeroA, numeroCero);
        }
    }
}
