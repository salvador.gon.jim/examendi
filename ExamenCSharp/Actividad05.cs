﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Actividad05
    {
        private string palabra;
        public Actividad05() {
            palabra = "Camaleon";
        }

        public void ejecutar()
        {
            Console.WriteLine();
            Console.WriteLine("Ejecutando actividad 5.");
            comprobarPalindromo();
            Console.WriteLine("Fin actividad 5.");
            Console.WriteLine();

        }

        public void comprobarPalindromo()
        {
           if (palabra == cadenaInversa())
            {
                Console.WriteLine("La palabra " + palabra + " Es palindromo");
            } else
            {
                Console.WriteLine("La palabra " + palabra + " No es palindromo");
            }
        }

        public string cadenaInversa()
        {
            string palabraInversa = "";
            for (int i = 1; i < palabra.Length + 1; i++)
            {
                palabraInversa += palabra.ElementAt(palabra.Length - i);
            }
            return palabraInversa;
        }
    }
}
