﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Vehiculo
    {
        protected string marca;
        protected string modelo;
        public Vehiculo() { }

        public Vehiculo(string marca, string modelo) {
            this.marca = marca;
            this.modelo = modelo;
        }   
        public virtual void informacionVehiculo()
        {
            Console.WriteLine(this.marca + " " + this.modelo);
        }
    }
}
