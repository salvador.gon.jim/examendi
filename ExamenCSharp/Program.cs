﻿using ExamenCSharp;

internal class Program
{
    private static void Main(string[] args)
    {
        Actividad01 actividad01 = new Actividad01();
        actividad01.ejecutar();
        Actividad02 actividad02 = new Actividad02();
        actividad02.ejecutar();
        Actividad03 actividad03 = new Actividad03();
        actividad03.ejecutar();
        Actividad04 actividad04 = new Actividad04();
        actividad04.ejecutar();
        Actividad05 actividad05 = new Actividad05();
        actividad05.ejecutar();
    }
}