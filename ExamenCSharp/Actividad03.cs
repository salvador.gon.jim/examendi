﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCSharp
{
    internal class Actividad03
    {
        Calculadora calculadora;
        public Actividad03() {
        calculadora = new Calculadora();
        }  

        public void ejecutar()
        {
            Console.WriteLine();
            Console.WriteLine("Ejecutando actividad 3.");
            pruebas();
            Console.WriteLine("Fin actividad 3.");
            Console.WriteLine();
        }

        public void pruebas()
        {
            int numeroA = 4;
            int numeroB = 7;
            int numeroCero = 0;
            calculadora.realizarPruebas(numeroA, numeroB, numeroCero);
        }
    }
}
